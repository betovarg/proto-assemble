// Animated anchor scrolling
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
})

$(document).ready(function() {

  //executes flexslider
  $('.flexslider').flexslider({
    animation: "slide",
    slideshowSpeed: 4000,
    animationSpeed: 1000,
    useCSS: true, 
    touch: true,
    pauseOnAction: true,
    controlNav: false,
    easing: 'easeInOutCubic',
  });

});

// back to top
$(document).scroll(function () {
    //Show element after user scrolls 800px
    var y = $(this).scrollTop();
    if (y > 500) {
        $('.back-to-top').addClass("visible");
    } else {
        $('.back-to-top').removeClass("visible");
    }
});

// waypoints example
// $(function() {
//   $('.store').waypoint(function() {
//     $(this).addClass("block-active");
//   }, { offset: '70%' });
// });