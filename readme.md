# Base para prototipado con Assemble.io

## Generalidades
- Bootstrap (https://github.com/assemble/boilerplate-bootstrap) para layouts, buttons, navigation, tipografía
- Gemfile de Manatí por @cienvaras
- Flexslider para montar slides
- /js/cripts.js contiene scripts comunes. Se puede ampliar a lo que se quiera

## Gruntfile y Assemble

### Grunt 
El gruntfile proveido contiene:
- Matchdep para cargar todos los plugins
- Connect para servir en localhost:8000
- Server y assemble corren en la tarea default

### Assemble 
- Templates en src/templates
  -  Layouts definen estructura de página
    - layout global: page.hbs
    - layout para inicio: page-front.hbs
    - layout para blog: page-blog.hbs
  -  Partials contenidos son header.hbs y footer.hbs
  -  Helpers opcionales (actualmente sirve lista con links de contenido)
- Posts
  - *Pendiente*

**Pendiente:**

- Livereload
- Compilar sass con bundle exec compass watch con grunt
- Minificado de html, css y js. Como herramienta es para prototipado, sería **opcional**.

## SASS

- config.rb coloca el css exportado, no se hace a través de grunt ()
- Se usa el gemfile provisto por @cienvaras (compass, breakpoint, singularity es opcional)
- Se carga bootstrap/bootstra-custom.scss, para comentar parciales que no se necesitan
- Dentro de partials:
  - Components
  - Pages, en caso de que se requiera un parcial por página
- Se debe compilar con *bundle exec compass watch*
